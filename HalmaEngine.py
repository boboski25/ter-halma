


class GameState():
    def __init__(self):
        #board est de dimension de 8*8, chaque élement à 2 caractères
        #le premier caractere reprensente la couleur, black ou white
        #"--" represente un espace vide dans le board
        self.board = [
            ["--","--","--","--","--","bp","bp","bp","--"],
            ["--","--","--","--","--","bp","bK","bp","--"],
            ["--","--","--","--","--","bp","bp","bp","--"],
            ["--","--","--","--","--","--","--","--","--"],
            ["--","--","--","--","--","--","--","--","--"],
            ["wp","wp","wp","--","--","--","--","--","--"],
            ["wp","wK","wp","--","--","--","--","--","--"],
            ["wp","wp","wp","--","--","--","--","--","--"],
            ["--","--","--","--","--","--","--","--","--"],
            ]
        self.whiteToMove = True
        self.moveLog =  []

        self.whiteWin = False
        self.blackWin = False

    def winCondition(self):
        self.whiteWin = True
        for i in range(3):
            for j in range(5,8):
                if self.board[i][j][0] != 'w':
                    self.whiteWin = False
                else:
                    if self.board[1][6] != "wK":
                        self.whiteWin = False

        self.blackWin = True
        for i in range(5,8):
            for j in range(3):
                if self.board[i][j][0] != "b":
                    self.blackWin = False
                else:
                    if self.board[6][1] != "bK":
                        self.blackWin = False
        return [self.whiteWin, self.blackWin]




    def makeMove(self, move):
        self.board[move.startRow][move.startCol] = "--"
        self.board[move.endRow][move.endCol] = move.pieceMoved
        self.moveLog.append(move)
        self.whiteToMove = not self.whiteToMove


    #revenir au mouvement précedent
    def undoMove(self):
        if len(self.moveLog) !=0: # etre sur deja que il y'a un mouvement
            move = self.moveLog.pop()
            self.board[move.startRow][move.startCol] = move.pieceMoved
            self.board[move.endRow][move.endCol] = move.pieceCaptured
            self.whiteToMove = not self.whiteToMove


    def getValidMoves(self):
        return self.getAllPossibleMoves()


    def getAllPossibleMoves(self):
        moves = []
        for r in range(len(self.board)):
            for c in range(len(self.board[r])):
                turn = self.board[r][c][0] #b ou w
                if (turn == 'w' and self.whiteToMove) or (turn == 'b' and not self.whiteToMove):
                    piece = self.board[r][c][1]
                    if piece == 'p':
                        self.getPawnMoves(r, c, moves)
                    elif piece == 'K':
                        self.getKingMoves(r,c, moves)

        return moves

    def getPawnMoves(self, r, c, moves):
        if self.whiteToMove:  #pion blanc
            if r-1 >=0:
                if self.board[r-1][c] == "--": #en avant
                    moves.append(Move((r,c),(r-1, c), self.board))

                if self.board[r-1][c] != "--" and self.board[r-2][c] == "--":
                    moves.append(Move((r,c),(r-2, c), self.board))

            if r+1 <= 7:
                if self.board[r+1][c] == "--" : #en arriere
                    moves.append(Move((r,c),(r+1, c),self.board))
                if self.board[r+1][c] != "--" and self.board[r+2][c] == "--" : #en arriere
                    moves.append(Move((r,c),(r+2, c),self.board))
            if c+1 <=7 :
                if self.board[r][c+1] == "--": #en droite
                    moves.append(Move((r,c),(r, c+1),self.board))
                if self.board[r][c+1] != "--" and self.board[r][c+2] == "--": #en droite
                    moves.append(Move((r,c),(r, c+2),self.board))
            if c-1 >= 0 :
                if self.board[r][c-1] == "--": #en gauche
                    moves.append(Move((r,c),(r, c-1),self.board))
                if self.board[r][c-1] != "--" and self.board[r][c-2] == "--": #en gauche
                    moves.append(Move((r,c),(r, c-2),self.board))



        else:

            if r-1 >=0:
                if self.board[r-1][c] == "--": #en avant
                    moves.append(Move((r,c),(r-1, c), self.board))

                if self.board[r-1][c] != "--" and self.board[r-2][c] == "--":
                    moves.append(Move((r,c),(r-2, c), self.board))

            if r+1 <= 7:
                if self.board[r+1][c] == "--" : #en arriere
                    moves.append(Move((r,c),(r+1, c),self.board))
                if self.board[r+1][c] != "--" and self.board[r+2][c] == "--" :
                    moves.append(Move((r,c),(r+2, c),self.board))
            if c+1 <=7 :
                if self.board[r][c+1] == "--": #en droite
                    moves.append(Move((r,c),(r, c+1),self.board))
                if self.board[r][c+1] != "--" and self.board[r][c+2] == "--":
                    moves.append(Move((r,c),(r, c+2),self.board))
            if c-1 >= 0 :
                if self.board[r][c-1] == "--": #en gauche
                    moves.append(Move((r,c),(r, c-1),self.board))
                if self.board[r][c-1] != "--" and self.board[r][c-2] == "--":
                    moves.append(Move((r,c),(r, c-2),self.board))







    def getKingMoves(self, r, c, moves):
        kingSimMoves = ((0,-1),(0,1),(-1,0),(1,0),(-1,-1),(-1,1),(1,1),(1,-1))
        KingHardMoves = ((0,-2),(0,2),(-2,0),(2,0),(-2,-2),(-2,2),(2,2),(2,-2))
        if self.whiteToMove:
            allyColor ='w'
            enemyColor = 'b'
        else:
            enemyColor = 'w'
            allyColor ='b'
        for move in kingSimMoves:
            endRow = r + move[0]
            endCol = c + move[1]
            for moveh in KingHardMoves:
                endRRow = r + moveh[0]
                endCCol = c + moveh[1]
                if 0 <= endRow < 8 and 0 <= endCol < 8:
                    endPiece = self.board[endRow][endCol]
                    endPPiece = self.board[endRRow][endCCol]
                    if endPiece[0] != allyColor and endPiece[0] != enemyColor:
                        moves.append(Move((r,c),(endRow,endCol),self.board))
                    if endPiece != "--" and endPPiece == "--":
                        moves.append(Move((r,c),(endRRow,endCCol),self.board))






class Move():

    ranksToRows = {"1": 7, "2": 6, "3": 5, "4": 4,
                   "5": 3,"6": 2, "7":1, "8": 0}
    rowsToRanks ={v: k for k, v in ranksToRows.items()}

    filesToCols = {"a": 0, "b": 1, "c": 2, "d": 3,
                   "e": 4,"f": 5, "g":6, "h": 7}

    colsToFiles ={v: k for k, v in filesToCols.items()}



    def __init__(self, startSq, endSq, board):
        self.startRow = startSq[0]
        self.startCol = startSq[1]
        self.endRow = endSq[0]
        self.endCol = endSq[1]
        self.pieceMoved = board[self.startRow][self.startCol]
        self.pieceCaptured = board[self.endRow][self.endCol]
        self.moveID = self.startRow * 1000 + self.startCol * 100 + self.endRow * 10 +self.endCol
        print(self.moveID)

    def __eq__(self, other):
        if isinstance(other, Move):
            return self.moveID == other.moveID

        return False


    def getChessNotation(self):
        return self.getRankFile(self.startRow, self.startCol) + self.getRankFile(self.endRow,self.endCol)


    def getRankFile(self, r, c):
        return self.colsToFiles[c] + self.rowsToRanks[r]
