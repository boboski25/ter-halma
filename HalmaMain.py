import pygame
import HalmaEngine



pygame.init()

WIDTH = 512
HEIGHT= 512
DIMENSION = 8
SQ_SIZE = HEIGHT // DIMENSION
MAX_FPS = 15
IMAGES = {}

screen = pygame.display.set_mode((WIDTH, HEIGHT))
clock = pygame.time.Clock()

green = (0, 200, 0)
red = (200, 0, 0)

bright_green = (0, 255, 0)
bright_red = (255, 0, 0)

pause = False
win = False
###les images
def loadImages():
    pieces = ['wp','bp','wK','bK']
    for piece in pieces:

        IMAGES[piece]= pygame.transform.scale(pygame.image.load(piece + ".png"),(SQ_SIZE,SQ_SIZE))
        #maintenant on peut appeler une image avec 'IMAGES['bp'] par exemple


#####

def text_objects(text, font):
    textSurface = font.render(text, True, pygame.Color("Black"))
    return textSurface, textSurface.get_rect()

### Création de la fonction bouton

def button (msg, x, y, w, h, ic, ac, action = None):

        mouse = pygame.mouse.get_pos()
        click = pygame.mouse.get_pressed()

        if x + w > mouse[0] > x and y + h > mouse[1] > y:
            pygame.draw.rect(screen, ac, (x, y, w, h))
            if click[0] == 1 and action != None :
                action()
        else:
            pygame.draw.rect(screen, ic, (x, y, w, h))

        smallText = pygame.font.Font('freesansbold.ttf', 20)
        TextSurf, TextRect = text_objects(msg, smallText)
        TextRect.center = ((x +(w/2)), (y + (h/2)))
        screen.blit(TextSurf, TextRect)

# création fonction quit

def quitgame() :
    pygame.quit()
    quit()

### création de la fonction pause
def unWin():
    global win
    win = False

def unpaused():
    global pause
    pause = False

def paused():

    background = pygame.image.load("img/wall.jpg")
    while pause:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
        screen.blit(background, (0,0))
        largeText = pygame.font.Font('freesansbold.ttf', 40)
        TextSurf, TextRect = text_objects("Paused", largeText)
        TextRect.center = ((WIDTH/2), (HEIGHT/5))
        screen.blit(TextSurf, TextRect)

        button ("Continue", 100, 370, 100, 40, green, bright_green, unpaused)
        button ("Quit", 300, 370, 100, 40, red, bright_red, quitgame)

        pygame.display.update()
        clock.tick(15)

def winWindow(text):

    background = pygame.image.load("img/wall.jpg")
    while win:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
        screen.blit(background, (0,0))
        largeText = pygame.font.Font('freesansbold.ttf', 40)
        TextSurf, TextRect = text_objects(text, largeText)
        TextRect.center = ((WIDTH/2), (HEIGHT/5))
        screen.blit(TextSurf, TextRect)

        button ("restart", 100, 370, 100, 40, green, bright_green, main)
        button ("Quit", 300, 370, 100, 40, red, bright_red, quitgame)

        pygame.display.update()
        clock.tick(15)

def game_intro():

    intro = True
    pygame.display.set_caption('Chess Halma')
    background = pygame.image.load("img/wall.jpg")

    while intro:

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()
        screen.blit(background, (0, 0))
        largeText = pygame.font.Font('freesansbold.ttf', 40)
        TextSurf, TextRect = text_objects("Chess Halma", largeText)
        TextRect.center = ((WIDTH/2), (HEIGHT/5))
        screen.blit(TextSurf, TextRect)

        button ("Play", 100, 370, 100, 40, green, bright_green, main)
        button ("Exit", 300, 370, 100, 40, red, bright_red, quitgame)

        pygame.display.update()
        clock.tick(15)


#### la création de main

def main():

    clock = pygame.time.Clock()
    screen.fill(pygame.Color("white"))
    gs = HalmaEngine.GameState()

    validMoves = gs.getValidMoves()
    moveMade = False
    global pause
    global win

    loadImages()
    running = True
    sqSelected = () #aucune case n'est selectionné, mais on trace la derniere click de l'utilsateur (tuple (row,column))
    playerClicks = [] # trace les cliques de l'utilisateurs , deux tuples [(x,y),(xx,yy)]

    while running:
        for e in pygame.event.get():
            if e.type == pygame.QUIT:
                running = False
                pygame.quit()
            elif e.type == pygame.MOUSEBUTTONDOWN:
                location = pygame.mouse.get_pos() #(x,y) location de la souris
                c = location[0]//SQ_SIZE
                r = location[1]//SQ_SIZE
                if sqSelected == (r, c): #l'utilisateur à cliquer deux fois sur la case
                    sqSelected = () #deselectionner la case
                    playerClicks = []
                else:
                    sqSelected = (r, c)
                    playerClicks.append(sqSelected) #on ajoute les coordonnées de la premiere et puis la deuxieme cliques

                if len(playerClicks) == 2 : # apres la deuxieme clique
                     move = HalmaEngine.Move(playerClicks[0], playerClicks[1], gs.board)
                     print(move.getChessNotation())
                     if move in validMoves:
                         gs.makeMove(move)
                         moveMade = True

                     sqSelected = () # reset les cliques de l'utilisateurs
                     playerClicks= []
            elif e.type == pygame.KEYDOWN:
                if e.key == pygame.K_z:
                    gs.undoMove()
                    moveMade = True
            if e.type == pygame.KEYDOWN:
                if e.key == pygame.K_p:
                    pause = True
                    paused()
        if moveMade:
            validMoves = gs.getValidMoves()
            moveMade= False

        drawGameState(screen,gs)
        clock.tick(MAX_FPS)
        pygame.display.flip()
        pygame.display.set_caption("Chess Halma")
        winState = gs.winCondition()
        print(winState)
        if True in winState:
            win = True
            if winState[0]:
                text = "white win!!"
            else:
                text = "black win!!"
            winWindow(text)




## graphique du jeu
def drawGameState(screen, gs):
    drawBoard(screen) #dessiner les cases sur le board
    drawPieces(screen, gs.board) # dessiner les pieces sur les cases

# l'ordre c'est dessiner le board et puis les pieces sur le board.
def drawBoard(screen):

    colors = [pygame.Color("moccasin"),pygame.Color("sienna")]
    for row in range(DIMENSION):
        for column in range(DIMENSION):
            color = colors[((row+column) % 2)]
            pygame.draw.rect(screen,color, pygame.Rect(column*SQ_SIZE, row*SQ_SIZE, SQ_SIZE, SQ_SIZE ))



def drawPieces(screen,board):
    for row in range(DIMENSION):
        for column in range(DIMENSION):
            piece = board[row][column]
            if piece != "--" :  #si la case n'est pas vide
                screen.blit(IMAGES[piece] , pygame.Rect(column*SQ_SIZE, row*SQ_SIZE, SQ_SIZE, SQ_SIZE ))

if __name__ == "__main__":
    game_intro()
    main()
    pygame.quit()
    quit()
